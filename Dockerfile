# registry.gitlab.com/c11k/genesis:latest
# For unit testing and deployment
# Set the base image for subsequent instructions
FROM phpdockerio/php72-fpm:latest

# Update packages
RUN apt-get update \
    && apt-get -y --no-install-recommends install php7.2-mysql php-redis php-xdebug \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process

# Install Laravel Envoy
RUN composer self-update && composer global require "laravel/envoy=~1.0" && composer clear-cache
