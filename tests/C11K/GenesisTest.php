<?php

namespace C11K;

use PHPUnit\Framework\TestCase;

class GenesisTest extends TestCase
{

    /** @var  Genesis $genesis */
    private $genesis;

    public function setUp()
    {
        $this->genesis = new Genesis();
    }

    /**
     * Test that the library is set correctly.
     */
    public function testIsValidReturnsTrue()
    {
        $this->assertEquals(true, $this->genesis->isValid());
    }
}
